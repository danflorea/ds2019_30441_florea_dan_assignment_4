﻿namespace MedClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sendSoapRequestButton = new System.Windows.Forms.Button();
            this.patientIdInput = new System.Windows.Forms.TextBox();
            this.soapResponseDisplayLabel = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // sendSoapRequestButton
            // 
            this.sendSoapRequestButton.Location = new System.Drawing.Point(12, 108);
            this.sendSoapRequestButton.Name = "sendSoapRequestButton";
            this.sendSoapRequestButton.Size = new System.Drawing.Size(208, 44);
            this.sendSoapRequestButton.TabIndex = 0;
            this.sendSoapRequestButton.Text = "Request Data";
            this.sendSoapRequestButton.UseVisualStyleBackColor = true;
            this.sendSoapRequestButton.Click += new System.EventHandler(this.Button1_Click);
            // 
            // patientIdInput
            // 
            this.patientIdInput.Location = new System.Drawing.Point(12, 80);
            this.patientIdInput.Name = "patientIdInput";
            this.patientIdInput.Size = new System.Drawing.Size(208, 22);
            this.patientIdInput.TabIndex = 1;
            this.patientIdInput.Text = "420";
            // 
            // soapResponseDisplayLabel
            // 
            this.soapResponseDisplayLabel.Location = new System.Drawing.Point(241, 12);
            this.soapResponseDisplayLabel.Name = "soapResponseDisplayLabel";
            this.soapResponseDisplayLabel.Size = new System.Drawing.Size(794, 424);
            this.soapResponseDisplayLabel.TabIndex = 2;
            this.soapResponseDisplayLabel.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1047, 448);
            this.Controls.Add(this.soapResponseDisplayLabel);
            this.Controls.Add(this.patientIdInput);
            this.Controls.Add(this.sendSoapRequestButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button sendSoapRequestButton;
        private System.Windows.Forms.TextBox patientIdInput;
        private System.Windows.Forms.RichTextBox soapResponseDisplayLabel;
    }
}

