﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace MedClient
{

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            var _url = "http://localhost:8080/ws";

            var input = "420";
            input = patientIdInput.Text;

            var xmlRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
                             "\r\nxmlns:med=\"http://danf.com/dsproject/soap\">\r\n    <soapenv:Header/>\r\n    " +
                             "<soapenv:Body>\r\n        <med:DoctorActivityRequest>\r\n            " +
                             "<med:patientId>" + input + "</med:patientId>\r\n        </med:DoctorActivityRequest>\r\n    " +
                             "</soapenv:Body>\r\n</soapenv:Envelope>";

            XMLHandler handler = new XMLHandler();
            var receivedResponseXML = handler.PostXMLData(_url, xmlRequest);

            

            XmlDocument response = new XmlDocument();
            response.LoadXml(receivedResponseXML);
            XmlNodeList xmlNodes = response.GetElementsByTagName("ns2:activities");
            List<PatientData> patientDatas = new List<PatientData>();

            foreach (XmlNode nd in xmlNodes)
            {
                XmlNode patientId = nd.ChildNodes[0];
                XmlNode activity = nd.ChildNodes[1];
                XmlNode startTimeX = nd.ChildNodes[2];
                XmlNode endTimeX = nd.ChildNodes[3];

                var startTime = (new DateTime(1970, 1, 1)).AddMilliseconds(double.Parse(startTimeX.InnerText));
                var endTime = (new DateTime(1970, 1, 1)).AddMilliseconds(double.Parse(endTimeX.InnerText));

                patientDatas.Add(new PatientData(
                        patientId.InnerText,
                        activity.InnerText,
                        startTime.ToString("R"),
                        endTime.ToString("R")
                    ));
            }

            soapResponseDisplayLabel.Text = "";

            foreach (PatientData pd in patientDatas)
            {
                pd.patientId.Replace("\t", " ");
                pd.activity.Replace("\t", " ");
                pd.startTime.Replace("\t", " ");
                pd.endTime.Replace("\t", " ");

                String toDisplay = pd.activity + " \t " + pd.startTime + " \t " + pd.endTime;
                soapResponseDisplayLabel.Text = soapResponseDisplayLabel.Text + " \n " + toDisplay;
            }

        }
    }
}
