﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedClient
{
    public class PatientData
    {
        public String patientId;
        public String activity;
        public String startTime;
        public String endTime;

        public PatientData(String patientId, String activity, String startTime, String endTime)
        {
            this.patientId = patientId;
            this.activity = activity;
            this.startTime = startTime;
            this.endTime = endTime;
        }
    }
}
