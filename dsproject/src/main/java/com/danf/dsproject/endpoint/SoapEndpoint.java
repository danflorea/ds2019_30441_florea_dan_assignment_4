package com.danf.dsproject.endpoint;

import com.danf.dsproject.service.SoapService;
import com.danf.dsproject.soap.DoctorActivityRequest;
import com.danf.dsproject.soap.DoctorActivityResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
@RequiredArgsConstructor
public class SoapEndpoint {

    private static final String NAMESPACE = "http://danf.com/dsproject/soap";
    private final SoapService service;

    @PayloadRoot(namespace = NAMESPACE, localPart = "DoctorActivityRequest")
    @ResponsePayload
    public DoctorActivityResponse getResponse(@RequestPayload DoctorActivityRequest request)
    {
        return service.getSoapActivities(request);
    }

}
