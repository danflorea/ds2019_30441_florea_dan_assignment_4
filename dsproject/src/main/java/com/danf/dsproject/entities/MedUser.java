package com.danf.dsproject.entities;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@ToString(of = {"uid", "name", "gender", "birthdate", "role"})
@AllArgsConstructor
@NoArgsConstructor
public class MedUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer uid;
    private String name;
    private String gender;
    private String birthdate;
    private String address;
    private String username;
    private String password;
    private String role;

    @ManyToMany
    @JoinTable(
        name="patient_caregiver",
        joinColumns = @JoinColumn(name = "caregiverId"),
        inverseJoinColumns = @JoinColumn(name = "patientId")
    )
    private List<MedUser> caregivers;

    @ManyToMany
    @JoinTable(
        name="patient_caregiver",
        joinColumns = @JoinColumn(name="patientId"),
        inverseJoinColumns = @JoinColumn(name="caregiverId")
    )
    private List<MedUser> patients;

    @OneToMany
    private List<MedicationPlan> medicationPlan;


    public MedUser(String name, String gender, String birthdate, String address, String username, String password, String role) {
        this.name = name;
        this.gender = gender;
        this.birthdate = birthdate;
        this.address = address;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public MedUser(String name, String gender, String birthdate, String address, String username, String password, String role, List<MedUser> patients) {
        this.name = name;
        this.gender = gender;
        this.birthdate = birthdate;
        this.address = address;
        this.username = username;
        this.password = password;
        this.role = role;
        this.patients = patients;
    }
}
