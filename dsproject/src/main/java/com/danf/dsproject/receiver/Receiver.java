package com.danf.dsproject.receiver;

import com.danf.dsproject.persistence.api.RepositoryFactory;
import com.danf.dsproject.persistence.jpa.JpaRepositoryFactory;
import com.danf.dsproject.receiver.PatientData;
import lombok.RequiredArgsConstructor;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@RequiredArgsConstructor
public class Receiver implements CommandLineRunner {

    private final static String QUEUE_NAME = "SALVE";
    private final JpaRepositoryFactory factory;

    @Override
    public void run(String... args) throws Exception {
        Pattern pattern = Pattern.compile("\\s*(\\w+)+\\s*\\:\\s*(\\w+(\\/\\w+)*)+");

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            JSONParser jsonParser = new JSONParser();
            try {
                List<JSONObject> receivedData = (ArrayList<JSONObject>)jsonParser.parse(message);
                List<PatientData> data = new ArrayList<>();
                receivedData.forEach(element -> {
                    PatientData newData = new PatientData(
                            element.get("patientId").toString(),
                            element.get("activity").toString(),
                            Long.parseLong(element.get("start").toString()),
                            Long.parseLong(element.get("end").toString()));
                    data.add(newData);
                    this.factory.createPatientDataRepository().save(newData);

                    Long activityDuration = newData.getEndTime() - newData.getStartTime();
                    if (newData.getActivity().matches(".*Sleeping.*"))
                    {
                        if (activityDuration > 43200000)
                        {
                            System.out.println("\tTOO MUCH SLEEP !!! Patient: " + newData.toString());
                        }
                    } else if (newData.getActivity().matches(".*Leaving.*"))
                    {
                        if (activityDuration > 43200000)
                        {
                            System.out.println("\tTOO MUCH TIME OUTSIDE !!! Patient: " + newData.toString());
                        }
                    } else if (newData.getActivity().matches(".*Toileting.*"))
                    {
                        if (activityDuration > 3600000)
                        {
                            System.out.println("\tTOO MUCH TIME IN BATHROOM !!! Patient: " + newData.toString());
                        }
                    }
                });

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        };
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });
    }
}
