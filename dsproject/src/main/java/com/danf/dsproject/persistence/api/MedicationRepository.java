package com.danf.dsproject.persistence.api;

import com.danf.dsproject.entities.MedUser;
import com.danf.dsproject.entities.Medication;

import java.util.List;
import java.util.Optional;

public interface MedicationRepository {

    Medication save (Medication medication);
    Optional<Medication> findById (Integer id);
    List<Medication> findAll();
    void remove (Medication medication);

}
