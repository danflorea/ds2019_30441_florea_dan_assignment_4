package com.danf.dsproject.persistence.api;

import com.danf.dsproject.entities.Medication;
import com.danf.dsproject.entities.MedicationPlan;

import java.util.List;
import java.util.Optional;

public interface MedicationPlanRepository {

    Medication save (MedicationPlan medicationPlan);
    List<MedicationPlan> findAll();
    void remove (MedicationPlan medicationPlan);

}
