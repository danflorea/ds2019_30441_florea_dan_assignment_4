package com.danf.dsproject.service;

import com.danf.dsproject.persistence.api.RepositoryFactory;
import com.danf.dsproject.soap.DoctorActivityRequest;
import com.danf.dsproject.soap.DoctorActivityResponse;
import com.danf.dsproject.soap.PatientData;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SoapService {

    private final RepositoryFactory repository;

    public DoctorActivityResponse getSoapActivities(DoctorActivityRequest request)
    {
        DoctorActivityResponse response = new DoctorActivityResponse();
        repository.createPatientDataRepository()
                .findAllByPatientId(Integer.toString(request.getPatientId()))
                .forEach(data -> {
                    PatientData d1 = new PatientData();
                    d1.setPatientId(data.getPatientId());
                    d1.setActivity(data.getActivity());
                    d1.setStartTime(data.getStartTime());
                    d1.setEndTime(data.getEndTime());
                    response.getActivities().add(d1);
                });
        return response;
    }

}
